import React, { useEffect, useState } from 'react'
import api from "../../../../api"
import HomeHotView from "../HomeHotView"

export default function HomeHotList(props) {
  const [hot1List, setHot1List] = useState([])
  const [hot2List, setHot2List] = useState([])
  const [city,setCity] = useState(props.cityName)

  useEffect(()=>{
    api.getHomtHot1({
      cityName: props.cityName
    }).then(res => {
      if(res.data.status === 200){
        // console.log(res.data.result);
        setHot1List(res.data.result)
        setCity(res.data.city)
      }
    })
  }, [])
  useEffect(()=>{
    api.getHomtHot2({
      cityName: props.cityName
    }).then(res => {
      if(res.data.status === 200){
        // console.log(res.data.result);
        setHot2List(res.data.result)
        setCity(res.data.city)
      }
    })
  }, [])

  return (
    <div>
      {
        hot1List.length > 0 ?
        <HomeHotView data={hot1List} city={city} title={"热门推荐"} /> :
        <div>等待加载数据</div>
      }
      {
        hot2List.length > 0 ?
        <HomeHotView data={hot2List} city={city} title={"新品推荐"} /> :
        <div>等待加载数据</div>
      }
    </div>
  )
}
