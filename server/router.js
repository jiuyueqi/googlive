const express = require("express");
const router = express.Router();
const homehot = require("./data/home/homehot")
const searchData = require("./data/search")
const Mock = require("mockjs");
const detailsData = require("./data/details")
const commentData = require("./data/comment")
const orderCommentData = require("./data/order")

const Random = Mock.Random


/**
 * 首页热门数据
 */
router.get("/home/hot1",(req,res) =>{
    const cityName = req.query.cityName
    console.log(cityName);
    res.send({
        status:200,
        result:homehot.hot1,
        city: cityName
    })
})

router.get("/home/hot2",(req,res) =>{
    const cityName = req.query.cityName
    console.log(cityName);
    res.send({
        status:200,
        result:homehot.hot2,
        city: cityName
    })
})

/* 
    搜索页面
*/
router.get("/search", (req, res) => {
    const search = req.query.search
    let data = Mock.mock({
        hasMore: true,
        'data|5': [{
            "id|+1":Random.integer(),
            title: Random.csentence(5, 8),
            houseType: "17/19层| 4室1厅 - 273.97 ㎡",
            price: "<h3>130000</h3>",
            rentType: Random.cword(2),
            img: Random.image('800x600', Random.color(), '#FFF', 'png', Random.cword(5))
        }]
    })
    res.send({
        status: 200,
        result: data
    })
})

// 详情页
router.get("/details", (req, res) =>{
    const id = req.query.id
    console.log(id);
    res.send(detailsData)
})


// 登录
router.post("/login",(req,res) =>{
    const { username,password } = req.body;
    if(username && password){
        res.send({
            status:200,
            token:"enjy23rsdfe3fsveq.23r23sfdvfv.asdfqf4ag34ghdfjtkjhq",
            nick:username
        })
    }else{
        res.send({
            status:400,
            msg:'用户名密码错误'
        })
    }
})

// 评价
router.get("/comment", (req, res)=>{
    const id = req.query.id
    console.log(id);
    res.send({
        status: 200,
        result: commentData
    })
})

// 订单评价
router.get("/order/comment",(req,res) =>{
    const username = req.query.username;
    console.log(username);
    res.send({
        status:200,
        result:orderCommentData
    })
})

// 评价
router.post("/order/submit/comment", (req, res) =>{
    const username = req.body.username
    const id = req.body.id
    const content = req.body.content
    console.log(username, id, content);
    res.send({
        status: 200,
        msg: "评价成功"
    })
})


/* 
    Mock测试
*/
/* router.get("/mock", (req, res) =>{
    let data = Mock.mock({
        'data|5': [{
            id: Random.integer(),
            title: Random.csentence( 5, 8 ),
            houseType: "17/19层| 4室1厅 - 273.97 ㎡",
            price: "<h3>130000</h3>",
            rentType: Random.cword(2),
            img: Random.image('800x600', Random.color(), '#FFF', 'png', Random.cword(5))
        }]
    })
    res.send(data)
}) */

module.exports = router;