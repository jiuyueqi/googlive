import { createRoot } from 'react-dom/client';
import "./assets/css/common.less"
import "./assets/css/font.css"
import "./assets/css/iconfont.css"
import "./assets/csstest/iconfont.css"
import AppRouter from './router';
import {Provider} from "react-redux"
import store from "./redux/store/index"
import "./utils/init"


const container = document.getElementById('root');
const root = createRoot(container);
root.render(
  <Provider store={store}>
    <AppRouter />
  </Provider>
);