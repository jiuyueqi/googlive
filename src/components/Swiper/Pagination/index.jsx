import React from 'react'
import "./style.less"
import classnames from "classnames"

export default function Pagination(props) {
  const arr = new Array(props.len).fill(1)
  let currentIndex = props.currentIndex
  return (
    <div className="swiper-pagination">
      <ul>
        {
          arr.map((ele,index)=>{
            return (
              <li className={classnames({'selected':currentIndex===index})} key={index}>
              </li>
            )
          })
        }
      </ul>
    </div>
  )
}
