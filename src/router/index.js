import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Layout from "../pages/Main/Layout";
import Home from "../pages/Main/Home";
import LifeService from "../pages/Main/LifeService";
import Shop from "../pages/Main/Shop";
import User from "../pages/Main/User";
import Error from "../pages/Main/Error";
import City from "../pages/City";
import Search from "../pages/Search";

import BottomNav from "../components/BottomNav";
import Details from "../pages/Details";
import Login from "../pages/Login";
import Order from "../pages/Order";

const AppRouter = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/city" component={City}></Route>
        <Route exact path="/details/:id" component={Details}></Route>
        <Route exact path="/login" component={Login}></Route>
        <Route exact path="/search/:keywords" component={Search}></Route>
        <Route exact path="/order" component={Order}></Route>
        <Layout path="/">
          <BottomNav />
          <Switch>
            <Route exact path="/" component={Home}></Route>
            <Route exact path="/life" component={LifeService}></Route>
            <Route exact path="/shop" component={Shop}></Route>
            <Route exact path="/user" component={User}></Route>
            {/* <Route exact path="*" component={Error}></Route> */}
          </Switch>
        </Layout>
      </Switch>
    </Router>
  );
};

export default AppRouter;
