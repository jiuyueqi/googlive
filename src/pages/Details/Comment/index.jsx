import React, { useEffect, useState } from 'react'
import CommentView from '../CommentView'
import api from "../../../api"
import LoadMore from "../../../components/LoadMore"


export default function Comment(props) {
  const [comment, setComment] = useState([])
  const [hsaMore, setHasMore] = useState(false)

  useEffect(()=>{
    http()
  }, [])

  const loadMoreHandle = () =>{
    // 加载更多
    http()
  }
  function http() {
    api.comment({ id: props.id }).then(res => {
      if (res.data.status === 200) {
          setComment(comment.concat(res.data.result.data))
          setHasMore(res.data.result.hasMore)
      }
    }).catch(error => {
        console.log(error);
    })
  }

  
  return (
    <div>
      {
        comment.length > 0?
        <CommentView data={comment} />:
        <div>等等数据加载...</div>
      }
      {
        hsaMore?
        <LoadMore onLoadMore={ loadMoreHandle } />:
        <div>没有数据了...</div>
      }
    </div>
  )
}
