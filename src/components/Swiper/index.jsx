import React, { useState } from 'react'
import "./style.less"
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import Pagination from "./Pagination"
const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

export default function Swiper(props) {

  const [index, setIndex] = useState(0) 
  const handleChangeIndex = (index) =>{
    // console.log(index);
    setIndex(index)
  }

  return (
    <div className="swiper">
      <AutoPlaySwipeableViews index={index} onChangeIndex={handleChangeIndex}>
        {
          props.banners.map((ele,index)=>{
            return (
              <div className="swiper-view" key={index}>
                <img src={ele} alt=""/>
              </div>
            )
          })
        }
      </AutoPlaySwipeableViews>
      <Pagination currentIndex={index} len={props.banners.length}></Pagination>
    </div>
  )
}
