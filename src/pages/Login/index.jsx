import React from 'react'
import LoginView from "./LoginView/index"
import {useDispatch} from "react-redux"
import * as loginActions from "../../redux/actions/login"


export default function Login() {

  const dispatch = useDispatch()

  const loginHandle = (user) =>{
    // console.log(user);
    // 写入到Redux
    dispatch(loginActions.setLogin(user))
    // 本地
    localStorage.setItem("goodlive",JSON.stringify(user))
    // 回到上一级页面
    window.history.back()
  }

  return (
    <div>
      <LoginView onLoginEvent={loginHandle} />
    </div>
  )
}
