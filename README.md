# GoodLive

## 技术栈

React + ReactHook + ReactRouter +Redux +Axios + Less +第三方

## 功能

1.首页展示 2.城市管理 3.搜索功能 4.上拉加载 5.详情页 6.收藏功能 7.订单评价

## 环境

1. React 脚手架
   2.Axios
   3.Less 支持

## Less 支持的配置

在 React 脚手架的环境中，默认支持 CSS/SASS/SCSS，需要手动配置 Less
1.npm run eject 2.安装依赖

```js
npm install --save-dev less less-loader
```

3.修改配置文件

<!-- 配置1 -->

```js
const lessRegex = /\.less$/;
const lessModuleRegex = /\.module\.less$/;
```

<!-- 配置二 -->

```js
{
   test: lessRegex,
   exclude: lessModuleRegex,
   use: getStyleLoaders(
     {
       importLoaders: 3,
       sourceMap: isEnvProduction
         ? shouldUseSourceMap
         : isEnvDevelopment,
       modules: {
         mode: 'icss',
       },
     },
     'less-loader'
   ),
   // Don't consider CSS imports dead code even if the
   // containing package claims to have no side effects.
   // Remove this when webpack adds a warning or an error for this.
   // See https://github.com/webpack/webpack/issues/6571
   sideEffects: true,
 },
 // Adds support for CSS Modules, but using SASS
 // using the extension .module.scss or .module.sass
 {
   test: lessModuleRegex,
   use: getStyleLoaders(
     {
       importLoaders: 3,
       sourceMap: isEnvProduction
         ? shouldUseSourceMap
         : isEnvDevelopment,
       modules: {
         mode: 'local',
         getLocalIdent: getCSSModuleLocalIdent,
       },
     },
     'less-loader'
   ),
},
```

## 配置网络请求
1.安装依赖 npm i --save axios
2.配置相关文件


## 配置初始化样式
1.初始化css
2.引入字体图标库：iconfont

## 实现首页展示
1. 创建页面 （Home/Shop/User/LifeService）
2. 创建路由
   - 安装依赖：npm i react-router-dom@5
   - 配置router
3. 底部导航
4. 图标
5. 顶部导航
6. 首页轮播图
   - 文档: https://react-swipeable-views.com/
   - 安装依赖 npm install --save react-swipeable-views
   - 指示器需要独立实现
7. 搭建服务器
   - 安装依赖
   - 跨域使用cors后台解决
   - 数据来自json文件
8. 首页列表数据展示
   - 组件分类
    - 智能组件: 处理数据，获取数据，过滤数据
    - 木偶组件: 视图适配

## 实现城市管理
1. 创建城市管理页面实现路由跳转 City
2. 实现路由嵌套，将共享底部导航的页面做成二级路由：Layout布局
3. 城市页面组件效果实现 PubHeader CurrentCity CityList
4. 集成Redux：通过它来存储城市页面，根据城市不同，UI渲染不同的结果
   - Store Reducers Action
   - 依赖
   ```js
   npm install --save redux
   npm install --save react-redux
   npm install --save-dev redux-devtools-extension
   ```
   - 创建redux流程
5. 关联redux，存储城市数据
6. 页面数据需要根据城市进行切换
7. 城市列表ABC形式
  - 安装依赖  npm i react-city-select


## 实现搜索功能
1. 创建搜索界面，配置路由跳转
   - 抽离搜索的input组件
   - 配置路由
   - 监听keycode
   - 路由跳转携带参数
      HOOK实现方式，useParams
2. 实现网络请求的接口
   - 后台数据有限，所以每次搜索返回相同的测试内容
3. 前台访问接口，获取数据
4. 关于列表数据渲染的注意点:
   - 我们以前都是直接在获取列表数据直接渲染，往后需要在Item去渲染每一个视图
   - 渲染html结构
5. 搜索头部实现
6. 上拉加载实现
   - 上拉加载封装组件
   - 1.监听滚动事件 2.滚动高度+视口高度 >= 容器列表高度
   - getBoundingClientRect 动态获取元素距离顶部距离 < 视口高度 = 加载数据
   - 节流与防抖
      防抖: 在一个期限值内，如果发生多次请求，以最后一次为准
      节流: 在一个期限值内，只发起一次请求
   - 回流重绘
      避免程序回流重绘
7. 加载更多数据
8. Mock.js
   - 模拟数据，完全随机化
   - 安装 `npm install mockjs`
9. 图片预处理

## 详情页
1. 创建页面，配置路由
2. 内存泄露
   - 在React中，事件、定时器、网络请求：在滚动事件还没有结束的时候，跳转到另外
     一个页面出现了问题，上一页页面还在滚动，我们已经跳转到别的页面了
   - 处理内存泄露：网络请求取消、事件取消、定时器清除
3. 收藏功能
   - 收藏功能是否允许收藏，取决于用户是否登录，用户登录则收藏，用户未登录，跳转
     到登录页面
   - 判定用户是否登录


## 登录功能
   - 功能的实现
   - 验证
   ```js
    npm i lodash
    npm i validator
    npm i classnames
   ```

## 收藏功能
   判断用户是否登录
   - 登录则可以进行收藏
   - 未登录则跳转至登录界面

## 详情页与评价切换和展示
   - Tabs切换
   - 视图显示

## 订单评价






