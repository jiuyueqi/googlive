import React from 'react'
import Item from "./Item"

export default function SearchListView(props) {
  // console.log(props.search);
  return (
    <div>
      {
        props.search.map((ele,index)=>{
          return (
            <Item data={ele} key={index} />
          )
        })
      }
    </div>
  )
}
