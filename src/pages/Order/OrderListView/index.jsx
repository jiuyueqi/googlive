import React from "react";
import Item from "./Item";

export default function OrderListView(props) {
  return (
    <div>
      {props.data.map((ele, index) => {
        return <Item user={props.user} key={index} data={ele}></Item>;
      })}
    </div>
  );
}
