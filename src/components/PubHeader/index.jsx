import React from 'react'
import {withRouter} from "react-router-dom"
import "./style.less"

function PubHeader(props) {

  const backHandle = () =>{
    // 返回上一页有两种方案
    // props.history.go(-1) 
    window.history.back()
  }

  return (
    <div id="common-header">
      <span className="back-icon" onClick={backHandle}>
        <i className="icon-chevron-left"></i>
      </span>
      <span>{props.title}</span>
    </div>
  )
}

export default withRouter(PubHeader)
