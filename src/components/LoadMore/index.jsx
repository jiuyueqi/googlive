import React, { useEffect, useRef, useState } from 'react'
import "./style.less"
import {withRouter} from "react-router-dom"

function LoadMore(props) {
  
  const more = useRef()
  const [loadTop, setLoadTop] = useState(10000)

  useEffect(()=>{
    // getBoundingClientReact
    // 视口高度
    let timer = null;
    const winHeight = document.documentElement.clientHeight;
    window.addEventListener("scroll", scorllHandle)
    function scorllHandle(){
      if(more.current) {
        setLoadTop(more.current.getBoundingClientRect().top)
        if(timer) {
          clearTimeout(timer)
        }else {
          timer = setTimeout(() => {
            if(winHeight>loadTop){
              // console.log("加载更多数据");
              props.onLoadMore()
            }
          }, 300);
        }
      }
    }
    
    return ()=>{
      // window.removeEventListener("scroll", scorllHandle)
      // clearTimeout(timer)
    }
  },[loadTop])

  return (
    <div className="load" ref={more}>
      加载更多
    </div>
  )
}

export default withRouter(LoadMore)
